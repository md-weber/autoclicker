import 'package:autoclicker/game/bloc/bloc.dart';
import 'package:flame/components.dart';
import 'package:flame_bloc/flame_bloc.dart';
import 'package:flutter/painting.dart';

class PickaxeInfoBox extends RectangleComponent
    with FlameBlocListenable<UpgradeCubit, UpgradeState> {
  PickaxeInfoBox()
      : super(
          children: [
            TextComponent(
              text: 'Pickaxe (Tap)',
              textRenderer: TextPaint(style: const TextStyle(fontSize: 16)),
              position: Vector2(6, 6),
            )
          ],
          position: Vector2(240, 30),
          size: Vector2(300, 46),
          paint: Paint()..color = const Color(0xFF6F4600),
        );
  TextComponent? levelTextComponent;
  TextComponent? levelInfoTextComponent;

  @override
  Future<void>? onLoad() {
    levelTextComponent = TextComponent(
      text: '1 Level',
      position: Vector2(6, 24),
      textRenderer: TextPaint(style: const TextStyle(fontSize: 16)),
    );
    levelInfoTextComponent = TextComponent(
      text: '1 Points / tap',
      position: Vector2(100, 24),
      textRenderer: TextPaint(style: const TextStyle(fontSize: 16)),
    );

    addAll([levelTextComponent!, levelInfoTextComponent!]);
    return super.onLoad();
  }

  @override
  void onNewState(UpgradeState state) {
    super.onNewState(state);
    levelTextComponent?.text = '${state.pickaxeLevel} Level';
    levelInfoTextComponent?.text = '${state.pickaxeLevel} points / tap';
  }
}
