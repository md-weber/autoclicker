import 'dart:ui';

import 'package:autoclicker/components/pickaxe/pickaxe_row.dart';
import 'package:flame/components.dart';
import 'package:flame/experimental.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';

class PickaxeUpgrade extends SpriteComponent with TapCallbacks {
  PickaxeUpgrade(this.levelUp)
      : super(
          position: Vector2(90, 30),
          size: Vector2(128, 40),
          children: [UpgradeText()],
        );

  void Function() levelUp;
  Sprite? activeSprite;
  Sprite? inactiveSprite;

  @override
  Future<void>? onLoad() async {
    final image = await Flame.images.load('level_up_button.png');
    final spriteSheet = SpriteSheet(image: image, srcSize: Vector2(64, 32));

    activeSprite = spriteSheet.getSprite(0, 0);
    inactiveSprite = spriteSheet.getSprite(1, 0);

    sprite = inactiveSprite;

    return super.onLoad();
  }

  @override
  void render(Canvas canvas) {
    sprite =
        findParent<PickaxeRow>()!.upgradeActive ? activeSprite : inactiveSprite;
    super.render(canvas);
  }

  @override
  void onTapUp(TapUpEvent event) {
    levelUp();
    super.onTapUp(event);
  }
}

class UpgradeText extends TextComponent {
  UpgradeText() : super(text: 'Level Up');

  @override
  Future<void>? onLoad() {
    position = Vector2(18, 6);

    return super.onLoad();
  }
}
