import 'package:autoclicker/components/pickaxe/pickaxe.dart';
import 'package:autoclicker/components/pickaxe/pickaxe_info.dart';
import 'package:autoclicker/components/pickaxe/pickaxe_upgrade.dart';
import 'package:autoclicker/game/bloc/bloc.dart';
import 'package:autoclicker/helpers/read_bloc.dart';
import 'package:flame/components.dart';
import 'package:flame_bloc/flame_bloc.dart';

class PickaxeRow extends RectangleComponent
    with FlameBlocListenable<ScoreCubit, double> {
  int pickaxeLevel = 1;
  bool upgradeActive = false;

  @override
  void onNewState(double state) {
    super.onNewState(state);
    upgradeActive = state >= 10;
  }

  @override
  Future<void>? onLoad() {
    addAll([
      Pickaxe(),
      PickaxeUpgrade(levelUp),
      PickaxeInfoBox(),
    ]);
    return super.onLoad();
  }

  // TODO(md-weber): This method could go somewhere else,
  // remove the state handling inside here
  void levelUp() {
    if (upgradeActive) {
      readBloc<ScoreCubit, double>().decreased(10);
      readBloc<UpgradeCubit, UpgradeState>().pickaxeUpgraded();
    }
  }
}
