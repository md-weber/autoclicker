import 'dart:ui';

import 'package:autoclicker/game/bloc/bloc.dart';
import 'package:autoclicker/helpers/helpers.dart';
import 'package:flame/components.dart';
import 'package:flame/experimental.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';

class Pickaxe extends SpriteAnimationComponent with TapCallbacks {
  Pickaxe()
      : super(
          size: Vector2.all(64),
          position: Vector2(10, 10),
          playing: true,
        );

  @override
  Future<void>? onLoad() async {
    final image = await Flame.images.load('pickaxe.png');
    final spriteSheet = SpriteSheet(image: image, srcSize: Vector2.all(150));

    animation = SpriteAnimation(
      [
        SpriteAnimationFrame(spriteSheet.getSprite(0, 0), 0.1),
        SpriteAnimationFrame(spriteSheet.getSprite(0, 1), 0.1),
      ],
      loop: false,
    );

    return super.onLoad();
  }

  @override
  Future<void> render(Canvas canvas) async {
    super.render(canvas);
  }

  @override
  void onTapUp(TapUpEvent event) {
    super.onTapUp(event);
    playing = true;
    readBloc<ScoreCubit, double>().increased(
      readBloc<UpgradeCubit, UpgradeState>().state.pickaxeLevel.toDouble(),
    );
    animation = animation!
      ..reversed()
      ..reset();
  }
}
