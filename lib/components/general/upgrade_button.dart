import 'dart:ui';

import 'package:autoclicker/game/bloc/score_cubit.dart';
import 'package:flame/components.dart';
import 'package:flame/experimental.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';
import 'package:flame_bloc/flame_bloc.dart';

class UpgradeButton extends SpriteComponent
    with TapCallbacks, FlameBlocListenable<ScoreCubit, double> {
  UpgradeButton(
    this.onButtonPress, {
    this.activeSprite,
    this.inactiveSprite,
    this.buttonText,
    super.position,
    super.size,
  }) : super(
          children: [
            _UpgradeButtonText(text: buttonText),
          ],
        );

  Sprite? activeSprite;
  Sprite? inactiveSprite;
  bool isActive = true;
  VoidCallback onButtonPress;
  String? buttonText;

  @override
  void onNewState(double state) {
    super.onNewState(state);
    isActive = state >= 50;
  }

  @override
  Future<void>? onLoad() async {
    if (activeSprite == null && inactiveSprite == null) {
      final image = await Flame.images.load('level_up_button.png');
      final spriteSheet = SpriteSheet(
        image: image,
        srcSize: Vector2(64, 32),
      );

      activeSprite = spriteSheet.getSprite(0, 0);
      inactiveSprite = spriteSheet.getSprite(1, 0);

      sprite = inactiveSprite;
    }

    return super.onLoad();
  }

  @override
  void render(Canvas canvas) {
    sprite = isActive ? activeSprite : inactiveSprite;
    super.render(canvas);
  }

  @override
  void onTapUp(TapUpEvent event) {
    if (isActive) onButtonPress();
    super.onTapUp(event);
  }
}

class _UpgradeButtonText extends TextComponent {
  _UpgradeButtonText({String? text}) : super(text: text ?? 'Level Up');

  @override
  Future<void>? onLoad() {
    position = Vector2(18, 6);

    return super.onLoad();
  }
}
