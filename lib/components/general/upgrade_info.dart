import 'package:flame/components.dart';
import 'package:flutter/painting.dart';

class UpgradeInfo extends RectangleComponent {
  UpgradeInfo({this.title = '', this.level = '', this.value = ''})
      : super(
          position: Vector2(240, 100),
          size: Vector2(300, 46),
          paint: Paint()..color = const Color(0xFF6F4600),
        );

  TextComponent? titleComponent;
  TextComponent? levelComponent;
  TextComponent? valueComponent;

  final String title;
  final String level;
  final String value;

  @override
  Future<void>? onLoad() {
    titleComponent = TextComponent(
      text: title,
      textRenderer: TextPaint(
        style: const TextStyle(fontSize: 16),
      ),
      position: Vector2(6, 6),
    );
    levelComponent = TextComponent(
      text: level,
      position: Vector2(6, 24),
      textRenderer: TextPaint(
        style: const TextStyle(fontSize: 16),
      ),
    );
    valueComponent = TextComponent(
      text: value,
      position: Vector2(100, 24),
      textRenderer: TextPaint(
        style: const TextStyle(fontSize: 16),
      ),
    );
    addAll([
      titleComponent!,
      levelComponent!,
      valueComponent!,
    ]);

    return super.onLoad();
  }
}
