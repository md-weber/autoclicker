import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';

class Mine extends RectangleComponent {
  Mine()
      : super(
          paint: Paint()..color = const Color(0x00FFFFFF),
          anchor: Anchor.bottomRight,
        );

  @override
  Future<void>? onLoad() async {
    final mineImage = await Flame.images.load('mine_entrance_sheet.png');
    final mineSpriteSheet = SpriteSheet(
      image: mineImage,
      srcSize: Vector2.all(32),
    );

    await addAll([
      _MineSprite(
        sprite: mineSpriteSheet.getSprite(0, 0),
        priority: 0,
      ),
      _MineSprite(
        sprite: mineSpriteSheet.getSprite(0, 1),
        priority: 2,
      )
    ]);
    return super.onLoad();
  }
}

class _MineSprite extends SpriteComponent {
  _MineSprite({super.sprite, super.priority})
      : super(anchor: Anchor.bottomRight);

  @override
  void onGameResize(Vector2 size) {
    super.onGameResize(size);
    this.size = Vector2.all(256);
    position = Vector2(size.x, size.y / 3 * 2);
  }
}
