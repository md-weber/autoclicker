import 'package:autoclicker/gen/assets.gen.dart';
import 'package:flutter/material.dart';

class UpgradeMenu extends StatelessWidget {
  const UpgradeMenu({super.key});

  static const flameOverlayId = 'flame-overlay-upgrade_menu';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DecoratedBox(
        decoration: const BoxDecoration(color: Colors.white60),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: ListView(
            children: [
              Material(
                color: Colors.transparent,
                child: Row(
                  children: [
                    const Placeholder(
                      fallbackHeight: 64,
                      fallbackWidth: 64,
                    ),
                    const SizedBox(width: 8),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text('Pickaxe'),
                        Text('1 Point per Second')
                      ],
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: SizedBox(
                        height: 64,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage(
                                Assets.images.buttonActive.path,
                              ),
                            ),
                          ),
                          child: const Center(
                            child: Text(
                              'LEVEL UP',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
