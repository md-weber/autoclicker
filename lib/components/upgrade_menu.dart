import 'package:autoclicker/components/miner/miner_row.dart';
import 'package:autoclicker/components/pickaxe/pickaxe_row.dart';
import 'package:flame/components.dart';
import 'package:flutter/rendering.dart';

class UpgradeMenu extends RectangleComponent {
  UpgradeMenu()
      : super(
          size: Vector2(600, 300),
          position: Vector2.all(10),
          children: [PickaxeRow(), MinerRow()],
          paint: Paint()..color = const Color(0x88FFA754),
        );
}
