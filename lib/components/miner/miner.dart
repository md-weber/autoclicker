import 'package:autoclicker/components/miner/behavior/walk_behavior.dart';
import 'package:autoclicker/game/game.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';
import 'package:flame_behaviors/flame_behaviors.dart';

class Miner extends Entity with HasGameRef<AutoClickerGame> {
  Miner()
      : super(
          behaviors: [WalkBehavior()],
          children: [_Miner()],
        );

  @override
  Future<void>? onLoad() {
    position = Vector2(0, gameRef.canvasSize.y - 100);
    return super.onLoad();
  }
}

class _Miner extends SpriteAnimationComponent {
  _Miner() : super(size: Vector2.all(128), priority: 1);

  double elapsedTime = 0;

  @override
  Future<void>? onLoad() async {
    final workerImage = await Flame.images.load('worker.png');
    final worker = SpriteSheet(
      image: workerImage,
      srcSize: Vector2.all(128),
    );

    animation = worker.createAnimation(row: 3, stepTime: 0.1, to: 4);

    return super.onLoad();
  }
}
