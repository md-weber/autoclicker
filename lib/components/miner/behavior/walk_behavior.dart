import 'package:autoclicker/components/miner/miner.dart';
import 'package:autoclicker/game/game.dart';
import 'package:flame/components.dart';
import 'package:flame_behaviors/flame_behaviors.dart';

class WalkBehavior extends Behavior<Miner> with HasGameRef<AutoClickerGame> {
  @override
  void update(double dt) {
    final sizeOfTheGame = gameRef.size.x;

    if (parent.x < sizeOfTheGame - 50) {
      parent
        ..x += 2
        ..position = Vector2(parent.x, parent.position.y);
    }

    if (parent.x >= sizeOfTheGame - 75) {
      parent.removeFromParent();
    }

    super.update(dt);
  }
}
