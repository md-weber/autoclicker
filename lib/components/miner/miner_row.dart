import 'package:autoclicker/components/general/upgrade_button.dart';
import 'package:autoclicker/components/general/upgrade_info.dart';
import 'package:autoclicker/components/mine.dart';
import 'package:autoclicker/components/miner/miner.dart';
import 'package:autoclicker/game/bloc/bloc.dart';
import 'package:autoclicker/helpers/read_bloc.dart';
import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/sprite.dart';

class MinerRow extends RectangleComponent {
  int workerAmount = 0;

  @override
  Future<void>? onLoad() {
    addAll([
      _WorkerThumbnail(),
      UpgradeButton(
        spawnWorker,
        position: Vector2(90, 100),
        size: Vector2(128, 40),
      ),
      UpgradeInfo(
        level: '0 miners',
        title: 'Miner',
        value: '0 points per second (pps)',
      )
    ]);
    return super.onLoad();
  }

  void spawnWorker() {
    final upgradeCubit = readBloc<UpgradeCubit, UpgradeState>()
      ..workerUpgraded();

    /// Adding the worker between the mine sprites to make him go
    /// "into" the [Mine]
    final mine = parent!.parent!.children.query<Mine>().first;
    final miner = Miner();
    mine.add(miner);

    readBloc<ScoreCubit, double>().decreased(50);

    /// Update the text in the [UpgradeInfo]
    final upgradeInfo = firstChild<UpgradeInfo>();
    if (upgradeInfo == null) return;
    final minerCount = upgradeCubit.state.minerCount;
    upgradeInfo.levelComponent?.text = '$minerCount miners';
    upgradeInfo.valueComponent?.text =
        '${minerCount * 5} points per second (pps)';
  }
}

class _WorkerThumbnail extends SpriteComponent {
  _WorkerThumbnail()
      : super(position: Vector2(-12, 48), size: Vector2.all(128));

  @override
  Future<void>? onLoad() async {
    final image = await Flame.images.load('worker.png');
    final spriteSheet = SpriteSheet(image: image, srcSize: Vector2.all(128));
    sprite = spriteSheet.getSprite(0, 0);
    return super.onLoad();
  }
}
