import 'package:autoclicker/game/bloc/score_cubit.dart';
import 'package:autoclicker/game/bloc/upgrade_cubit.dart';
import 'package:autoclicker/game/bloc/upgrade_state.dart';
import 'package:autoclicker/helpers/helpers.dart';
import 'package:flame/components.dart';
import 'package:flame_behaviors/flame_behaviors.dart';

class PointUpdateBehavior extends Behavior {
  PointUpdateBehavior();

  double pointsInCurrentSecond = 0;

  @override
  Future<void>? onLoad() {
    add(TimerComponent(period: 1, onTick: calcPoints, repeat: true));
    return super.onLoad();
  }

  void calcPoints() {
    final workerAmount =
        readBloc<UpgradeCubit, UpgradeState>().state.minerCount;
    pointsInCurrentSecond = (workerAmount * 5) / 60;
  }

  @override
  void update(double dt) {
    super.update(dt);
    readBloc<ScoreCubit, double>().increased(pointsInCurrentSecond);
  }

  void increasePoints() {
    final workerAmount =
        readBloc<UpgradeCubit, UpgradeState>().state.minerCount;
    readBloc<ScoreCubit, double>().increased((5 * workerAmount).toDouble());
  }
}
