import 'package:autoclicker/components/point_counter/point_update_behavior.dart';
import 'package:autoclicker/game/bloc/score_cubit.dart';
import 'package:flame/components.dart';
import 'package:flame_behaviors/flame_behaviors.dart';
import 'package:flame_bloc/flame_bloc.dart';
import 'package:flutter/rendering.dart';

class PointCounter extends Entity {
  PointCounter()
      : super(
          behaviors: [PointUpdateBehavior()],
          children: [
            _PointTextComponent(
              text: '0 points',
              textRenderer: TextPaint(
                style: const TextStyle(color: Color(0xFF000000)),
              ),
            )
          ],
        );

  @override
  void onGameResize(Vector2 size) {
    super.onGameResize(size);
    position = Vector2(size.x - 100, 20);
  }
}

class _PointTextComponent extends TextComponent
    with FlameBlocListenable<ScoreCubit, double> {
  _PointTextComponent({super.text, super.textRenderer});

  @override
  void onNewState(double state) {
    super.onNewState(state);
    text = "${state.toStringAsFixed(0)} ${state == 1 ? "point" : "points"}";
  }
}
