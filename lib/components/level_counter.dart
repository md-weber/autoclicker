import 'package:flame/components.dart';
import 'package:flutter/material.dart';

class LevelCounter extends TextComponent {
  LevelCounter()
      : super(
          text: '1 Level',
          textRenderer: TextPaint(style: const TextStyle(color: Colors.black)),
        );

  @override
  void onGameResize(Vector2 size) {
    super.onGameResize(size);
    position = Vector2(size.x - 100, 0);
  }
}
