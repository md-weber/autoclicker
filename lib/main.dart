import 'package:autoclicker/game/bloc/bloc.dart';
import 'package:autoclicker/game/game.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

import 'components/ingame_ui/view/upgrade_menu.dart';

void main() {
  runApp(
    GameWidget(
      game: AutoClickerGame(
        scoreCubit: ScoreCubit(),
        upgradeCubit: UpgradeCubit(),
      ),
      initialActiveOverlays: const [UpgradeMenu.flameOverlayId],
      overlayBuilderMap: {
        UpgradeMenu.flameOverlayId: (_, __) => const UpgradeMenu()
      },
    ),
  );
}
