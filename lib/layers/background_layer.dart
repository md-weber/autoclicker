import 'package:flame/extensions.dart';
import 'package:flame/game.dart';
import 'package:flame/layers.dart';
import 'package:flame/sprite.dart';

class BackgroundLayer extends PreRenderedLayer {
  BackgroundLayer(this.sprites, this.size);

  final List<Sprite> sprites;
  final Vector2 size;

  @override
  void drawLayer() {
    for (final sprite in sprites) {
      sprite.render(
        canvas,
        position: Vector2.all(0),
        size: Vector2(size.x, size.y / 3 * 2),
      );
    }
  }
}
