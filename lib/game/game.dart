import 'dart:ui';

import 'package:autoclicker/components/level_counter.dart';
import 'package:autoclicker/components/mine.dart';
import 'package:autoclicker/components/point_counter/point_counter.dart';
import 'package:autoclicker/components/upgrade_menu.dart';
import 'package:autoclicker/game/bloc/score_cubit.dart';
import 'package:autoclicker/game/bloc/upgrade_cubit.dart';
import 'package:autoclicker/game/bloc/upgrade_state.dart';
import 'package:autoclicker/layers/background_layer.dart';
import 'package:flame/components.dart';
import 'package:flame/experimental.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame_behaviors/flame_behaviors.dart';
import 'package:flame_bloc/flame_bloc.dart';

class AutoClickerGame extends FlameGame with HasTappableComponents {
  AutoClickerGame({
    required ScoreCubit scoreCubit,
    required UpgradeCubit upgradeCubit,
  })  : _scoreCubit = scoreCubit,
        _upgradeCubit = upgradeCubit;

  final ScoreCubit _scoreCubit;
  final UpgradeCubit _upgradeCubit;

  LevelCounter? levelCounter;
  int points = 0;
  int pickaxeLevel = 1;
  BackgroundLayer? backgroundLayer;
  List<Sprite>? backgroundSprites;

  @override
  Color backgroundColor() => const Color(0xFFFFFFFF);

  @override
  Future<void>? onLoad() async {
    await super.onLoad();

    final backgrounds = await Flame.images.loadAll(
      ['background_1.png', 'background_2.png', 'background_3.png', 'background_4.png', 'background_5.png', 'background_6.png'],
    );

    backgroundSprites = List<Sprite>.generate(
      6,
      (index) => Sprite(backgrounds[index]),
    );

    backgroundLayer = BackgroundLayer(backgroundSprites!, size);

    await add(
      FlameMultiBlocProvider(
        providers: [
          FlameBlocProvider<ScoreCubit, double>.value(value: _scoreCubit),
          FlameBlocProvider<UpgradeCubit, UpgradeState>.value(
            value: _upgradeCubit,
          )
        ],
        children: [
          GameEntity(game: this, size: size),
        ],
      ),
    );
  }

  @override
  void onGameResize(Vector2 canvasSize) {
    super.onGameResize(canvasSize);
    if (backgroundSprites != null) {
      backgroundLayer = BackgroundLayer(backgroundSprites!, canvasSize);
    }
  }

  @override
  void render(Canvas canvas) {
    backgroundLayer?.render(canvas);
    super.render(canvas);
  }
}

class GameEntity extends Entity {
  GameEntity({
    required this.game,
    super.size,
    super.behaviors,
    super.children,
  });

  final AutoClickerGame game;

  @override
  Future<void>? onLoad() async {
    await super.onLoad();

    await addAll([
      Mine(),
      UpgradeMenu(),
      PointCounter(),
    ]);
  }
}
