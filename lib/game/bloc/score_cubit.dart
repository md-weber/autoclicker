import 'package:flutter_bloc/flutter_bloc.dart';

class ScoreCubit extends Cubit<double> {
  ScoreCubit() : super(0);

  void increased(double byPoints) {
    emit(state + byPoints);
  }

  void decreased(double byPoints) {
    emit(state - byPoints);
  }
}
