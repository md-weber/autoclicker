import 'package:autoclicker/game/bloc/upgrade_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UpgradeCubit extends Cubit<UpgradeState> {
  UpgradeCubit() : super(UpgradeState());

  void pickaxeUpgraded() {
    emit(state.copyWith(pickaxeLevel: state.pickaxeLevel + 1));
  }

  void workerUpgraded() {
    emit(state.copyWith(minerCount: state.minerCount + 1));
  }
}
