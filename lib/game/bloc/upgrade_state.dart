class UpgradeState {
  UpgradeState({
    this.minerCount = 0,
    this.pickaxeLevel = 1,
    this.isMinerUpgradeActive = false,
    this.isPickaxeUpgradeActive = false,
  });

  bool isMinerUpgradeActive;

  bool isPickaxeUpgradeActive;

  int minerCount;

  int pickaxeLevel;

  UpgradeState copyWith({
    int? minerCount,
    int? pickaxeLevel,
    bool? isMinerUpgradeActive,
    bool? isPickaxeUpgradeActive,
  }) {
    return UpgradeState(
      minerCount: minerCount ?? this.minerCount,
      pickaxeLevel: pickaxeLevel ?? this.pickaxeLevel,
      isMinerUpgradeActive: isMinerUpgradeActive ?? this.isMinerUpgradeActive,
      isPickaxeUpgradeActive:
          isPickaxeUpgradeActive ?? this.isPickaxeUpgradeActive,
    );
  }
}
